#include "TH1.h"
#include<vector>
#include "TCanvas.h"   
#include "TFile.h"  
#include "TStyle.h"
#include "TF1.h"           
#include "TGraphErrors.h"
#include "TMinuit.h"
#include"TVirtualFitter.h"
#include"TMatrixD.h"  
#include"TFitResult.h"
#include<fstream>
#include<iomanip>
#include"TLegend.h"
#include"TText.h"
#include"TLatex.h"
#include"TPaveStats.h"
using namespace std;

struct M{
  int Name;
  double Energy;
  int rebin;
};

struct SeperateProfiles
{
  TH1D* PhotonsLead;
  TH1D* ElectronsLead;
  TH1D* PositronsLead;
  TH1D* PhotonsScintillator;
  TH1D* ElectronsScintillator;
  TH1D* PositronsScintillator;
};

vector<SeperateProfiles> Profiles(15);
vector<M> Control(15);
double nEvents=10000;
int main()
{ 
  
  
  TCanvas *c[15];
  ofstream outt;
  outt.open ("results.txt");
  ofstream selected;
  selected.open("selectedresults.txt");
   ofstream ErrorInv("Errors.txt");
  
  for(int i=0; i<15;i++)
  {
    Control[i].Name=i+1;
    
  }
  Control[0].Energy=100.0;
  Control[1].Energy=44.44;
  Control[2].Energy=25.00;
  Control[3].Energy=16.00;
  Control[4].Energy=11.11;
  Control[5].Energy=8.16;
  Control[6].Energy=6.25;
  Control[7].Energy=4.94;
  Control[8].Energy=4.00;
  Control[9].Energy=3.31;
  Control[10].Energy=2.78;
  Control[11].Energy=2.37;
  Control[12].Energy=2.04;
  Control[13].Energy=1.78;
  Control[14].Energy=1.56;
  
  Control[14].rebin=6;
  Control[13].rebin=5;
  Control[12].rebin=5;
  Control[11].rebin=5;
  Control[10].rebin=5;
  Control[9].rebin=4;
  Control[8].rebin=4;
  Control[7].rebin=3;
  Control[6].rebin=2;
  Control[5].rebin=2;
  Control[4].rebin=2;
  Control[3].rebin=2;
  Control[2].rebin=2;
  Control[1].rebin=1;
  Control[0].rebin=1;
  
    
  
  TH1D* h1[15]; 
  TH1D* h2[15];
  TH1D* l1[15];
  TH1D* l2[15];
  //TH1D* N[15];
  TH1D* hnew[15];
  TH1D* H1;
  vector<double> PMean;
  vector<double> AMean;
  vector<double> PMeanError;
  vector<double> AMeanError;
  vector<double> PSigma;
  vector<double> PSigmaError;
  vector<double> ASigmaError;
  vector<double> ASigma;
  vector<double> CovElement;
  //vector<double> SF;
  //vector<double>SFError;
  
  
  //c1->Divide(2,2);
  for(int i=1;i<14;i++) //loop over all simulation output files
  {
    
    Char_t input[50];
    sprintf(input,"NOUTPUT_%d.root",Control[i].Name);
    TFile* In= new TFile(input);  
    //get histograms from files.
    h1[i]=(TH1D*)In->Get("1"); 
    h2[i]=(TH1D*)In->Get("2");
    l1[i]=(TH1D*)In->Get("11");
    l2[i]=(TH1D*)In->Get("12");
    hnew[i]= (TH1D*)h2[i]->Clone("hnew");
    hnew[i]->Rebin(Control[i].rebin);
    cout<<"nbins after cloning=  "<<hnew[i]->GetNbinsX()<<endl;
    
    Profiles[i].PhotonsLead=(TH1D*)In->Get("23");
    Profiles[i].ElectronsLead=(TH1D*)In->Get("24");
    Profiles[i].PositronsLead=(TH1D*)In->Get("25");
    Profiles[i].PhotonsScintillator=(TH1D*)In->Get("26");
    Profiles[i].ElectronsScintillator=(TH1D*)In->Get("27");
    Profiles[i].PositronsScintillator=(TH1D*)In->Get("28");
    Char_t Phl[50];
    Char_t El[50];
    Char_t Pol[50];
    Char_t Phs[50];
    Char_t Es[50];
    Char_t Pos[50];
    sprintf(Phl,"v9.6.p04 Photons in Lead Shower Profile at %.2f GeV",Control[i].Energy);
    sprintf(El,"v9.6.p04 Electrons in Lead Shower Profile at %.2f GeV",Control[i].Energy);
    sprintf(Pol,"v9.6.p04 Positrons in Lead Shower Profile at %.2f GeV",Control[i].Energy);
    sprintf(Phs,"v9.6.p04 Photons in Scintillator Shower Profile at %.2f GeV",Control[i].Energy);
    sprintf(Es,"v9.6.p04 Electrons in Scintillator Shower Profile at %.2f GeV",Control[i].Energy);
    sprintf(Pos,"v9.6.p04 Positrons in Scintillator Shower Profile at %.2f GeV;",Control[i].Energy);
    
    Profiles[i].PhotonsLead->SetTitle(Phl);
    Profiles[i].ElectronsLead->SetTitle(El);
    Profiles[i].PositronsLead->SetTitle(Pol);
    Profiles[i].PhotonsScintillator->SetTitle(Phs);
    Profiles[i].ElectronsScintillator->SetTitle(Es);
    Profiles[i].PositronsScintillator->SetTitle(Pos);
    
    //set titles of histograms
    Char_t titleActive[50];
    Char_t titlePassive[50];
    sprintf(titleActive,"Scintillator E- %f GeV",Control[i].Energy);
    sprintf(titlePassive,"Lead E - %f GeV",Control[i].Energy);
    //fit gaussians to histograms
    TFitResultPtr g= h1[i]->Fit("gaus","S");
    gStyle->SetOptFit();
    h1[i]->SetTitle(titlePassive);
    TF1 *fit1=h1[i]->GetFunction("gaus");

    //get mean, sigma and uncertainties from fit to gaussians for lead layers
    PMean.push_back(fit1->GetParameter(1));
    PMeanError.push_back(fit1->GetParError(1));
    PSigma.push_back(fit1->GetParameter(2));
    PSigmaError.push_back(fit1->GetParError(2));
    TFitResultPtr g2;
    double element;
    TF1* range = new TF1("range","gaus",0.1,0.3);
    g2=hnew[i]->Fit("gaus","S");
    TVirtualFitter *fitter = TVirtualFitter::GetFitter();
    element=fitter->GetCovarianceMatrixElement(2,1); //get covariance matrix element
    CovElement.push_back(element);
    gStyle->SetOptFit();
    hnew[i]->SetTitle(titleActive);
    TF1 *fit2=hnew[i]->GetFunction("gaus");
    
    //get mean and sigma for scintillator layers
    AMean.push_back(fit2->GetParameter(1)); 
    AMeanError.push_back(fit2->GetParError(1));
    ASigma.push_back(fit2->GetParameter(2));
    ASigmaError.push_back(fit2->GetParError(2));

  }
  
  
  double SigmaOverE[15]; //sigma over E
  double Xaxis[15]; //1 / sqrt(E)
  double SigmaOverEError[15];
  double XaxisError[15]; //=0
 
  double XB[15]; //Fille with Energy specified in .mac file
  double XBE[15];
  
  for(int k=0;k<13;k++)   //arrays run from 0 to 12.
  {
    
    SigmaOverE[k]=ASigma[k]/AMean[k];
    Xaxis[k]=1/sqrt(Control[k+1].Energy);  //change when 100GeV point removed
    SigmaOverEError[k]=(1/AMean[k])*sqrt(pow(ASigmaError[k],2)+pow((ASigma[k]*AMeanError[k]/(AMean[k])),2)+((2*ASigma[k]*
CovElement[k])/AMean[k]));
    cout<<"CovElement= "<<CovElement[k]<<endl;
    
   
    XaxisError[k]=0;
    XB[k]=Control[k+1].Energy;
    XBE[k]=0;
    
  }
  TFile* out= new TFile("Save.root","RECREATE");
  TCanvas*CStraightRes = new TCanvas("CStraightRes","CStraightRes",1800,1000);
  
  for(int l=1;l<14;++l)
  {
    Profiles[l].PhotonsLead->Write();
    Profiles[l].ElectronsLead->Write();
    Profiles[l].PositronsLead->Write();
    Profiles[l].PhotonsScintillator->Write();
    Profiles[l].ElectronsScintillator->Write();
    Profiles[l].PositronsScintillator->Write();
  }
  
  
   //------------1/sqrt(E)------------------------------------------------------------------------
  TF1* fitstraight= new TF1("fitstraight","sqrt(([0]*x)**2+(([1])**2))",0,2);
  fitstraight->SetParName(0,"a");
  fitstraight->SetParName(1,"c");
  fitstraight->SetParameter(0,0.01);
  fitstraight->SetParameter(1,0.01);
  TGraphErrors*Resolution1=new TGraphErrors(13,Xaxis,SigmaOverE,XaxisError,SigmaOverEError);
  Resolution1->Draw("AP");
  Resolution1->SetTitle("10000 events,Physics List=emstandard_opt1noapplycuts;");
  Resolution1->Fit("fitstraight"); //perform fit
  Resolution1->GetXaxis()->SetTitle("#frac{1}{#sqrt{E_{Incident}}}[GeV]");
  Resolution1->GetYaxis()->SetTitle("#frac{#sigma}{E}");
  Resolution1->GetYaxis()->SetTitleOffset(1.1);
  /*TText* Res1Text= new TText(0.1,0.1," #Sigma=Resolution of"
 "Energy Distribution in Scintillator layers E= Total Energy Deposited in"
 "Active Layers");*/
  TLatex* Res1Text= new TLatex();
  CStraightRes->cd();
  Resolution1->Draw("AP");
  //Add Labels
  Res1Text->SetTextSize(0.03);
  Res1Text->DrawLatex(0.13,0.064,"#sigma=Resolution of Energy Distribution in Active Layers");
  TText * Res1Line2= new TText();
  Res1Line2->SetTextSize(0.03);
  Res1Line2->SetTextSize(0.03);
  Res1Line2->DrawText(0.13,0.059,"E= Total Energy Deposited in Active Layers");
  TLatex* FitEquation= new TLatex();
  FitEquation->DrawLatex(0.13,0.052,"Fit: #frac{#sigma}{E}=#frac{a}{#sqrt{E}} #oplus C");
  FitEquation->SetTextSize(0.03);
  gStyle->SetOptStat();
  TPaveStats*ps= (TPaveStats*)CStraightRes->GetPrimitive("stats");
  ps->SetX1NDC(0.13);
  ps->SetX2NDC(0.47);
  ps->SetY1NDC(0.86);
  ps->SetY2NDC(0.76);
  // fitstraight->Draw();
  CStraightRes->Write();
  //write to root file
  Resolution1->Write("Resolution1");
  //output to text file
  selected<<"Term,Value,Uncertainty;"<<endl;
  selected<<"a, "<<setprecision(3)<<fitstraight->GetParameter(0)<<" , "<<setprecision(1)<<fitstraight->GetParError(0)<<";"<<endl;
  selected<<"c, "<<setprecision(1)<<fitstraight->GetParameter(1)<<" , "<<setprecision(1)<<fitstraight->GetParError(1)<<";"<<endl;
  selected<<"\n"<<endl;
  selected<<"Energy,Sampling Fraction,Uncertainty"<<endl;

  //-------------------------------Curved Fit----------------------------------
  TGraphErrors*Resolution2=new TGraphErrors(13,XB,SigmaOverE,XBE,SigmaOverEError);
  TCanvas*CResolution2=new TCanvas("CResolution2","CResolution2",1800,1000);
 
  TF1* fit1= new TF1("fit1","sqrt(([0]/sqrt(x))**2+(([1])**2))",0,2);
  fit1->SetParName(0,"a");
  fit1->SetParName(1,"c");
 
  fit1->SetParameter(0,0.01);
  fit1->SetParameter(1,0.01);
  fit1->SetLineColor(kBlue);
  Resolution2->Fit("fit1");
  
  Resolution2->SetTitle("10000 events, Physics List=emstandard_opt1noapplycuts");
  Resolution2->GetXaxis()->SetTitle("E_{Incident}[GeV]");
  Resolution2->GetYaxis()->SetTitle("#frac{#sigma}{E}");
  Resolution2->Draw("AP");
  gStyle->SetOptStat();
 TLatex* Res2Text= new TLatex();
  Res1Text->SetTextSize(0.03);
  Res1Text->DrawLatex(23.5,0.062,"#sigma=Resolution of Energy Distribution in Active Layers");
  TText * Res2Line2= new TText();
  Res2Line2->SetTextSize(0.03);
  Res2Line2->SetTextSize(0.03);
  Res2Line2->DrawText(23.5,0.057,"E= Total Energy Deposited Per Event in Active Layers");
   TLatex* FitEquation2= new TLatex();
  FitEquation2->DrawLatex(23.5,0.052,"Fit: #frac{#sigma}{E}=#frac{a}{#sqrt{E}} #oplus C");
  FitEquation2->SetTextSize(0.03);
  //write to root file
  Resolution2->Write("Resolution2");
  CResolution2->Write();
  //numerical results are inferred from straight line plot but are consistent with cureved plot.

  //----------------------------shower profiles-----------------------------------------
  double En[15];
  double SFError[15];
  double SF[15];
  double SFErrorProp[15];
  double LeadSF[15];
  double ScintSF[15];
  double LeadSFError[15];
  double ScintSFError[15];
  double Energ[15];
  double EnergyEr[15];for(int n=1;n<14;++n){
    int k=n-1;
    Double_t xmin=l2[n]->GetXaxis()->GetXmin();
    Double_t xmax=l2[n]->GetXaxis()->GetXmax();
    Double_t integral=l2[n]->Integral(xmin,xmax);
    Double_t xminlead=l1[n]->GetXaxis()->GetXmin();
    Double_t xmaxlead=l1[n]->GetXaxis()->GetXmax();
    Double_t integrallead=l1[n]->Integral(xminlead,xmaxlead);
    Double_t leadGeV=integrallead/1000.0;
    Double_t ScGeV=integral/1000.0;
   
  
    gStyle->SetOptStat(0);
   
    // l1[n]->GetYaxis()->SetTitle("#frac{dE}{dX}");
    //  l1[n]->GetXaxis()->SetTitle("Distance of Material Traversed/Layers");
    // l1[n]->Draw();
    // l2[n]->Draw("Same");
   
    
    //Leg[n]->AddEntry(l1[n],"Lead","P");
    //Leg[n]->AddEntry(l2[n],"Scintillator","P");
   
    En[k]=Control[n].Energy;
  //Sampling->Fit("pol1")`
    
       SF[k]=integral/integrallead;
       LeadSF[k]=leadGeV/(Control[n].Energy);
       ScintSF[k]=ScGeV/(Control[n].Energy);
       LeadSFError[k]=(PSigma[k]/sqrt(nEvents))*(1/Control[n].Energy);
       ScintSFError[k]=(ASigma[k]/sqrt(nEvents))*(1/Control[n].Energy);
       //SFError[k]=SF[k]*sqrt(pow(((ASigma[k]/sqrt(nEvents))/ScGeV),2)+pow(((PSigma[k]/sqrt(nEvents)))/leadGeV,2));
       SFError[k]=(ASigma[k]/sqrt(nEvents))*(1/leadGeV);
       ErrorInv<<"SFError= "<<SFError[k]<<"k= "<<k<<endl;
       ErrorInv<<"Asigma= "<<ASigma[k]<<"for k= "<<k<<endl;
       
       
    cout<<"Active Integral for Histogram "<<n<<"=  "<<ScGeV<<endl;
    cout<<"Absorber Integral for Histogram "<<n<<"=  "<<leadGeV<<endl;
    cout<<"sampling fraction for energy "<<Control[n].Energy<<"GeV = "<<ScGeV/leadGeV<<endl;
    cout<<"--------------------------------"<<endl;
    outt<<"Active Integral for Histogram "<<n<<"=  "<<ScGeV<<endl;
    outt<<"Absorber Integral for Histogram "<<n<<"=  "<<leadGeV<<endl;
    outt<<"sampling fraction for energy "<<Control[n].Energy<<"GeV = "<<ScGeV/leadGeV<<endl;
    outt<<"--------------------------------"<<endl;
    selected<<setprecision(4)<<Control[n].Energy<<" , "<<setprecision(4)<<SF[k]<<" , "<<setprecision(2)<<SFError[k]<<";"<<endl;
  }
  TGraphErrors* ScintSampling =new TGraphErrors(13,En,ScintSF,0,ScintSFError);
  ScintSampling->Write("ScintSampling");
  TGraphErrors* LeadSampling = new TGraphErrors(13,En,LeadSF,0,LeadSFError);
  LeadSampling->Write("LeadSampling");
  //create sampling fraction plot and write it to file.
  TGraphErrors*Sampling=new TGraphErrors(13,En,SF,0,SFError);
  Sampling->SetTitle("Sampling Fraction as a Funciton of Energy, Phys List=emstandard_opt1noapplycuts,10000 events");
  Sampling->GetXaxis()->SetTitle("E/GeV");
  Sampling->GetYaxis()->SetTitle("#frac{Scintillator E}{Lead E}");
  // Sampling->Fit("pol1");
  Sampling->Write("Sampling");
  TCanvas* CSampling = new TCanvas("CSampling","CSampling",1800,1000);
  Sampling->GetYaxis()->SetTitleOffset(1.2);
  Sampling->Draw("AP");
  gPad->SetLogx();
  CSampling->Write();

  
  TLegend*Leg[15];
 //write longitudinal showers to file and sort formatting for shower profiles
  for(int j=1;j<14;j++)
  {
    Char_t canvasname[20];
    sprintf(canvasname,"c_%d",j);
    c[j]=new TCanvas(canvasname,canvasname,1800,1000);
   
    Char_t title1[50];
    Char_t title2[50];
    Char_t longitudinaltitle[50];
    sprintf(title1,"1_%d",Control[j].Name);
    sprintf(title2,"2_%d",Control[j].Name);
    sprintf(longitudinaltitle,"Energy deposited per event, %5.2f GeV incident energy",Control[j].Energy);
    h1[j]->Write(title1);
    h2[j]->Write(title2);
    hnew[j]->Write("Hnew");
    l2[j]->SetLineColor(kRed);
    l1[j]->SetLineColor(kBlue);
    l1[j]->SetTitle(longitudinaltitle);
    l2[j]->SetTitle(longitudinaltitle);
    l1[j]->GetXaxis()->SetTitle("Layer number");
    l2[j]->GetXaxis()->SetTitle("Layer number");
    l1[j]->GetYaxis()->SetTitle("#frac{MeV}{Layer}");
    l2[j]->GetYaxis()->SetTitle("#frac{MeV}{Layer}");
    l1[j]->Write();
    l2[j]->Write();
    c[j]->cd();
    l1[j]->Draw();
    l2[j]->Draw("SAME");
    Leg[j]=new TLegend(0.75,0.60,0.95,0.75);
    Leg[j]->AddEntry(l1[j],"Lead","L");
    Leg[j]->AddEntry(l2[j],"Scintillator","L");
    Leg[j]->Draw();
    c[j]->SetLogy();
    c[j]->Write();
    
    //N[j]->Write();
    // hnew[j]->Write();
  }
  
  

 
  for(int E=0;E<13;++E)
  {
    ErrorInv<<"Psigma for E = "<<E<<" "<<PSigma.at(E)<<endl;
  }
  
  //output selected results to file
  TFile* Selectedroot= new TFile("Selectedresults.root","RECREATE");
  CStraightRes->Write();
  CResolution2->Write();
  CSampling->Write();
  for(int n=1;n<14;++n){
    c[n]->Write();
  }
  selected.close();
  outt.close();
  //cout<<"this was compiled with my script"<<endl;
  
}
