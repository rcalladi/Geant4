#!/bin/bash

CurrentDir="${PWD##*/}"
echo $CurrentDir
TestDir=CaloTest
if [[ ! -d "CaloTest" && $CurrentDir != $TestDir ]]; then
mkdir CaloTest
cd CaloTest
fi

if [[ -d "CaloTest" && $CurrentDir != $TestDir ]]; then
cd CaloTest
fi

testEm3.exe $G4TESTEM3ROOT/scripts/opt1noapplycuts.mac

#There is a bug with ROOT 6 opening Geant4 created histograms, have to use old root for now
# amazurov: Currentlu I could not observe this problem: 
# source /afs/cern.ch/sw/lcg/app/releases/ROOT/5.34.30/$CMTCONFIG/root/bin/thisroot.sh

g++ -c `root-config --cflags` $G4TESTEM3ROOT/scripts/Plot.C
g++ -o Plot `root-config --glibs` Plot.o

./Plot

echo "Results Produced in Save.root, Selectedresults.root and selectedresults.txt"


