#!/usr/bin/env python
#
#  copyPatchedSource.py
#  Author   Nigel Watson 14 Feb 2012
#
#  Copy private G4 headers from package to install area
#
import  os, sys, fnmatch, shutil

def main():    
# Dir in which we keep updated source/headers.
 # SRCNEW_Dir="../srcnew"
  #SRCNEW_cross_sections_Dir = "../srcnew/cross_sections"
#11/2014  SRCNEW_diffraction_Dir="../srcnew/diffraction"
#11/2014  SRCNEW_management_Dir="../srcnew/management"


# Dir for original source
#11/2014  SRC_Dir="../hadronic/models/chiral_inv_phase_space/interface/src"
# Dir for original FTF source to be replaced
#11/2014  SRC_diffraction_Dir="../hadronic/models/parton_string/diffraction/src"
#11/2014  SRC_management_Dir="../hadronic/models/parton_string/management/src"
  #SRC_cross_sections_Dir="../"
# Can we get cmt macro values in .python?
# Expanded by cmt to be $(GEANT4_home)/$(GEANT4_installarea_prefix)/include
#  INSTALLAREA_project=os.environ['GEANT4_install_include']
#  INSTALLAREA_package=os.environ['G4PROCESSESROOT']+'/G4processes'
  #INSTALLAREA_project = "../../../InstallArea/include"
  #INSTALLAREA_package = "../G4processes"

#$(G4processes_root)/G4processes

# Find any files in the srcnew dir (CHIPS cross-section fixes)
#11/2014  for newfile in os.listdir(SRCNEW_Dir):
#11/2014    if fnmatch.fnmatch(newfile, '*.hh'):
#11/2014#Replace old headers in install areas with new.
#11/2014      fname = os.path.join(SRCNEW_Dir, newfile)
#11/2014      shutil.copy2(fname,INSTALLAREA_project)
#11/2014      shutil.copy2(fname,INSTALLAREA_package)
#11/2014# Replace old .cc with new.
#11/2014    if fnmatch.fnmatch(newfile, '*.cc'):
#11/2014      fname = os.path.join(SRCNEW_Dir, newfile)
#11/2014      shutil.copy2(fname,SRC_Dir)


#11/2014# Find any files in the srcnew dir (FTF mass problem fixes)
#11/2014  for newfile in os.listdir(SRCNEW_diffraction_Dir):
#11/2014# Only .cc to replace for FTF fix.
#11/2014#    if fnmatch.fnmatch(newfile, '*.hh'):
#11/2014#Replace old headers in install areas with new.
#11/2014#      fname = os.path.join(SRCNEW_Dir, newfile)
#11/2014#      shutil.copy2(fname,INSTALLAREA_project)
#11/2014#      shutil.copy2(fname,INSTALLAREA_package)
#11/2014# Replace old .cc with new.
#11/2014    if fnmatch.fnmatch(newfile, '*.cc'):
#11/2014      fname = os.path.join(SRCNEW_diffraction_Dir, newfile)
#11/2014      shutil.copy2(fname,SRC_diffraction_Dir)

#11/2014# Find any files in the srcnew dir (string max. retries problem fixes)
#11/2014  for newfile in os.listdir(SRCNEW_management_Dir):
#11/2014# Only .cc to replace for max. retries fix.
#11/2014#    if fnmatch.fnmatch(newfile, '*.hh'):
#11/2014#Replace old headers in install areas with new.
#11/2014#      fname = os.path.join(SRCNEW_Dir, newfile)
#11/2014#      shutil.copy2(fname,INSTALLAREA_project)
#11/2014#      shutil.copy2(fname,INSTALLAREA_package)
#11/2014# Replace old .cc with new.
#11/2014    if fnmatch.fnmatch(newfile, '*.cc'):
#11/2014      fname = os.path.join(SRCNEW_management_Dir, newfile)
#11/2014      shutil.copy2(fname,SRC_management_Dir)

# Find any files in the srcnew dir (fixes missing assert in LHCb CLHEP version)
  #for newfile in os.listdir(SRCNEW_cross_sections_Dir):
# Only .cc to replace for CLHEP fix.
#    if fnmatch.fnmatch(newfile, '*.hh'):
#Replace old headers in install areas with new.
#      fname = os.path.join(SRCNEW_Dir, newfile)
#      shutil.copy2(fname,INSTALLAREA_project)
#      shutil.copy2(fname,INSTALLAREA_package)
# Replace old .cc with new.
   # if fnmatch.fnmatch(newfile, '*.cc'):
    #  fname = os.path.join(SRCNEW_cross_sections_Dir, newfile)
     # shutil.copy2(fname,SRC_cross_sections_Dir)

#========================================TESTEM3 PRIVATE SRC COPYING========================================
  SRCNEW_DIR="../srcnew/"
  SRC_DIR="../src/"

  for privatesource in os.listdir(SRCNEW_DIR):
    if privatesource!='.svn':
      fname=os.path.join(SRCNEW_DIR,privatesource)
      shutil.copy2(fname,SRC_DIR)

if __name__ == "__main__":
    main()
