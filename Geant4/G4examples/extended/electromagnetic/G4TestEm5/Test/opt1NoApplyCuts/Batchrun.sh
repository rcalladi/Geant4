source /afs/cern.ch/lhcb/software/releases/LBSCRIPTS/prod/InstallArea/scripts/LbLogin.sh

SetupProject Geant4 HEAD --nightly lhcb-gauss-dev --user-area /afs/cern.ch/work/t/tiwillia/private/9.6p04cmtuser/

python /afs/cern.ch/work/t/tiwillia/private/9.6p04cmtuser/Geant4_HEAD/Xamples/extended/electromagnetic/G4TestEm5/x86_64-slc6-gcc48-opt/testEm5.exe /afs/cern.ch/work/t/tiwillia/private/9.6p04cmtuser/Geant4_HEAD/Xamples/extended/electromagnetic/G4TestEm5/Test/opt1NoApplyCuts/MacGen.py 1000

/afs/cern.ch/work/t/tiwillia/private/9.6p04cmtuser/Geant4_HEAD/Xamples/extended/electromagnetic/G4TestEm5/x86_64-slc6-gcc48-opt/testEm5.exe /afs/cern.ch/work/t/tiwillia/private/9.6p04cmtuser/Geant4_HEAD/Xamples/extended/electromagnetic/G4TestEm5/Test/opt1NoApplyCuts/BruteForce.mac 1000

cp RMSResults.root /afs/cern.ch/work/t/tiwillia/private/9.6p04cmtuser/Geant4_HEAD/Xamples/extended/electromagnetic/G4TestEm5/Test/opt1NoApplyCuts/run6RMSResults.root

cp Output* /afs/cern.ch/work/t/tiwillia/private/9.6p04cmtuser/Geant4_HEAD/Xamples/extended/electromagnetic/G4TestEm5/Test/opt1/run6Outputs/
