import sys
n=int(sys.argv[1])
macout=file('BruteForce.mac','w')
macout.write('/control/verbose 2 \n/run/verbose 2 \n/testem/det/setAbsMat Silicon \n/testem/det/setAbsThick 300 um \n/testem/det/setAbsYZ    42 mm \n/testem/phys/addPhysics  emstandard_opt1nocuts \n/testem/phys/setCuts 5 mm \n')
Energies=[1,2,3,4,5,7,9,12,15,20,25,30,40]
Limits=[4.0,3.0,2.0,1.5,1.0,0.6,0.5,0.5,0.4,0.2,0.2,0.15,0.15]
for E in Energies:
    j=0
    for i in range(0,n):
        macout.write('/run/initialize \n/testem/gun/setDefault \n/gun/particle e- \n/gun/energy %.2f GeV \n/analysis/setFileName Output_%.2f \n/analysis/h1/set  1 100 0 100 keV       #energy depostied in absorber \n/analysis/h1/set  10 100 0 100 keV      #K.E at exit of world \n/analysis/h1/set  11 100 0 100 keV      #Energy fluence dE/dOmega \n/analysis/h1/set  12 100 0.0 0.09 mrad          #space angle dN/dOmega \n/analysis/h1/set  13 100 -%.2f %.2f mrad        #projected angle at exit of world \n/analysis/h1/set  14 100 -14 14 nm      #projected positon at exit of world \n/analysis/h1/set  15 100 0 44 mm        #radius at exit of world \n/run/beamOn 10000 \n'%(E,E,Limits[j],Limits[j]))
    j=j+1
