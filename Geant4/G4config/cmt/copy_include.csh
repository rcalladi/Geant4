#!/usr/bin/env csh

#set echo on

set here = $PWD

cd ../../..

set incl_dir = $G4SHARE/include/

if !( -d InstallArea/$CMTCONFIG/include ) then
  mkdir -p InstallArea/$CMTCONFIG

  cd InstallArea/$CMTCONFIG

  $G4_UNIX_COPY ${incl_dir} .

  # MCl: there may be a symlink called 'Geant4' and pointing to '.'
  #      we need to avoid that it is dereferenced, but we need to preserve the structure
  if ( -e include/Geant4 ) then
    rm -rf include/Geant4
    mkdir -p include/Geant4
    $G4_UNIX_COPY ${incl_dir}. include/Geant4/.
    rm -rf include/Geant4/Geant4
  endif

  echo ' include files have been copied from '${incl_dir}
else
  echo ' include files exist - NO copy from '${incl_dir}
endif

cd $here



