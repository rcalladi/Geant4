#!/usr/bin/env csh
#nkw 21-jun-2012
# expat available on most linux, and also in LCG external area, but propose to adopt the version packaged within G4 to reduce unnecessary external dependences.
# Initially, set up for copying only expat, but anticipate we re-use this to use the g4 provided version of clhep as well.
set echo on
#copy files from G4Dir = externals
set G4Dir = "externals"

set here = $PWD
 
set pack = `cmt -quiet show macro_value package`
if ( "$pack" == "G4externalslcg" ) then
   set p = "expat/src"
endif  


cd $here/..

if !( -d $p) then
  mkdir -p $p
  echo "about to..." 
  echo $G4_UNIX_COPY ${G4SRC}/${G4Dir}/${p}/*.* ${p}/.
  echo  $G4_UNIX_COPY ${G4SRC}/${G4Dir}/${p}/../include/*.* ${pack}/.

  $G4_UNIX_COPY ${G4SRC}/${G4Dir}/${p}/*.* ${p}/.
  $G4_UNIX_COPY ${G4SRC}/${G4Dir}/${p}/../include/*.* ${pack}/.
  echo " Source files have been copied from ${G4SRC}/{$G4Dir}/${p}"
else
  echo "  ${G4SRC}/${p} already exists, skipped copy"
endif

cd $here

unset echo

