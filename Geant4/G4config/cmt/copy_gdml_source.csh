#!/usr/bin/env csh
# NKW  18-Jul-2012
# Non-standard copy script required to get GDML sources from G4 release area.
#set echo on
set G4Dir = "persistency/gdml"

set here = $PWD
set list = ""
set pack = `cmt -quiet show macro_value package`
if ( "$pack" == "G4GDML" ) then
   set list = "src schema"
endif  

cd $here/..

foreach p ($list[*])
  if !( -d $p) then
   mkdir -p $p
   $G4_UNIX_COPY ${G4SRC}/${G4Dir}/${p}/*.* ${p}/.
# Easier to just copy includes a second time.
   $G4_UNIX_COPY ${G4SRC}/${G4Dir}/${p}/../include/*.* ${pack}/.
   echo "Source files have been copied from ${G4SRC}/{$G4Dir}/${p}"
 else
   echo "${G4SRC}/${p} already exists, skipped copy"
 endif
end

cd $here

#unset echo

