# Aliases (LHCb names -> G4 names)

set(parmodels_source parameterisations)

foreach(pack analysis digits_hits event geometry global graphics_reps intercoms
             materials parmodels particles physics_lists processes readout run
             track tracking)
  if(${pack}_source)
    set(src_pack ${${pack}_source})
  else()
    set(src_pack ${pack})
  endif()
  message(STATUS "Copying ${src_pack} sources to G4${pack}")
  execute_process(COMMAND find ( -type d -a -name src
                                 -a -not -path "*test/*" )
                               #-printf "copying ${src_pack}/%p\\n"
                               -exec mkdir -p "@G4DST@/G4${pack}/{}" ";"
                               -exec cp -r "{}/." "@G4DST@/G4${pack}/{}" ";"
                  WORKING_DIRECTORY @G4SRC@/${src_pack})
  execute_process(COMMAND find ( -type d -a -name include
                                 -a -not -path "*test/*" )
                               #-printf "copying ${pack}/%p\\n"
                               -exec mkdir -p "@G4DST@/G4${pack}/G4${pack}" ";"
                               -exec cp -r "{}/." "@G4DST@/G4${pack}/G4${pack}" ";"
                  WORKING_DIRECTORY @G4SRC@/${src_pack})
endforeach()

# UI packages
foreach(pack basic common GAG)
  message(STATUS "Copying interfaces/${pack} sources")
  execute_process(COMMAND mkdir -p @G4DST@/G4UI${pack}/${pack}/src
                                   @G4DST@/G4UI${pack}/G4UI${pack})
  execute_process(COMMAND cp -r @G4SRC@/interfaces/${pack}/src/.
                                @G4DST@/G4UI${pack}/${pack}/src)
  execute_process(COMMAND cp -r @G4SRC@/interfaces/${pack}/include/.
                                @G4DST@/G4UI${pack}/G4UI${pack})
endforeach()

# Vis packages
foreach(pack FukuiRenderer modeling OpenGL RayTracer Tree
             management VRML)
  if(pack STREQUAL "FukuiRenderer")
    set(local_pack G4FR)
  elseif(pack STREQUAL "management")
    set(local_pack G4vis_management)
  else()
    set(local_pack G4${pack})
  endif()
  message(STATUS "Copying visualization/${pack} sources to ${local_pack}")
  execute_process(COMMAND mkdir -p @G4DST@/${local_pack}/${pack}/src
                                   @G4DST@/${local_pack}/${local_pack})
  execute_process(COMMAND cp -r @G4SRC@/visualization/${pack}/src/.
                                @G4DST@/${local_pack}/${pack}/src)
  execute_process(COMMAND cp -r @G4SRC@/visualization/${pack}/include/.
                                @G4DST@/${local_pack}/${local_pack})
endforeach()

set(pack externals)
message(STATUS "Copying visualization/${pack} sources to G4${pack}")
execute_process(COMMAND find ( -type d -a -name src
                                 -a -not -path "*test/*" )
                               #-printf "copying ${pack}/%p\\n"
                               -exec mkdir -p "@G4DST@/G4${pack}/externals/{}" ";"
                               -exec cp -r "{}/." "@G4DST@/G4${pack}/externals/{}" ";"
                WORKING_DIRECTORY @G4SRC@/visualization/${pack})
execute_process(COMMAND find ( -type d -a -name include
                                 -a -not -path "*test/*" )
                               #-printf "copying ${pack}/%p\\n"
                               -exec mkdir -p "@G4DST@/G4${pack}/G4${pack}" ";"
                               -exec cp -r "{}/." "@G4DST@/G4${pack}/G4${pack}" ";"
                WORKING_DIRECTORY @G4SRC@/visualization/${pack})


# Vis packages
set(pack gdml)
message(STATUS "Copying persistency/${pack} sources to ${local_pack}")
set(local_pack G4GDML)
execute_process(COMMAND mkdir -p @G4DST@/${local_pack}/src
                                 @G4DST@/${local_pack}/schema
                                 @G4DST@/${local_pack}/${local_pack})
execute_process(COMMAND cp -r @G4SRC@/persistency/${pack}/src/.
                              @G4DST@/${local_pack}/src)
execute_process(COMMAND cp -r @G4SRC@/persistency/${pack}/schema/.
                              @G4DST@/${local_pack}/schema)
execute_process(COMMAND cp -r @G4SRC@/persistency/${pack}/include/.
                              @G4DST@/${local_pack}/${local_pack})


# Examples
foreach(ex_name TestEm3 TestEm5 TestEm7 TestEm9)
  message(STATUS "Copying examples/.../${ex_name} sources to G4examples/.../G4${ex_name}")
  set(src @G4SRC@/../examples/extended/electromagnetic/${ex_name})
  set(dest @G4DST@/G4examples/extended/electromagnetic/G4${ex_name})
  execute_process(COMMAND mkdir -p ${dest}/include ${dest}/src)
  execute_process(COMMAND cp -r ${src}/include/. ${dest}/include)
  execute_process(COMMAND cp -r ${src}/src/. ${dest}/src)
  execute_process(COMMAND cp -r ${src}/${ex_name}.cc ${dest})
endforeach()

foreach(ex_name Hadr00)
  message(STATUS "Copying examples/.../${ex_name} sources to G4examples/.../G4${ex_name}")
  set(src @G4SRC@/../examples/extended/hadronic/${ex_name})
  set(dest @G4DST@/G4examples/extended/hadronic/G4${ex_name})
  execute_process(COMMAND mkdir -p ${dest}/include ${dest}/src)
  execute_process(COMMAND cp -r ${src}/include/. ${dest}/include)
  execute_process(COMMAND cp -r ${src}/src/. ${dest}/src)
  execute_process(COMMAND cp -r ${src}/${ex_name}.cc ${dest})
endforeach()
