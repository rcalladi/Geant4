# This configuration is not really for the package, but
# to build Geant4.

CMAKE_MINIMUM_REQUIRED(VERSION 2.8.5)

if(CMAKE_BUILD_TYPE STREQUAL "Release")
  set(G4VERBOSE OFF)
else()
  set(G4VERBOSE ON)
endif()

if(NOT G4DATA AND Geant4Files_XENV)
  execute_process(COMMAND ${env_cmd} --xml ${Geant4Files_XENV} printenv G4DATA
                  OUTPUT_VARIABLE G4DATA)
  string(STRIP "${G4DATA}" G4DATA)
  if(G4DATA)
    message(STATUS "Using G4DATA from ${G4DATA}")
    set(G4DATA "${G4DATA}" CACHE PATH "Directory with Geant4 data")
  endif()
else()
  set(G4DATA ../share)
endif()

if (NOT GEANT4_TAG)
  if(DEFINED ENV{G4_NATIVE_VERSION})
    set(GEANT4_TAG $ENV{G4_NATIVE_VERSION})
    if(NOT GEANT4_TAG MATCHES "^4\\.")
      # G4_NATIVE_VERSION might not contain the initial '4.' that we need
      # see https://its.cern.ch/jira/browse/LHCBGAUSS-43
      set(GEANT4_TAG 4.${GEANT4_TAG})
    endif()
    if(NOT GEANT4_TAG MATCHES "^geant")
      set(GEANT4_TAG geant${GEANT4_TAG})
    endif()
  else()
    set(GEANT4_TAG ${CMAKE_PROJECT_VERSION})
  endif()
  # Special mapping of version HEAD
  if(GEANT4_TAG STREQUAL "HEAD")
    set(GEANT4_TAG master)
  endif()
  set(GEANT4_TAG ${GEANT4_TAG} CACHE STRING "Tag of Geant4 to get from the Geant4-srcs repository")
endif()

include(ExternalProject)

ExternalProject_Add(Geant4
   GIT_REPOSITORY ${GEANT4_SRCS_REPOSITORY}
   GIT_TAG ${GEANT4_TAG}
   CMAKE_ARGS -DCMAKE_TOOLCHAIN_FILE=${CMAKE_SOURCE_DIR}/toolchain.cmake
              -DGEANT4_BUILD_CXXSTD=${GAUDI_CXX_STANDARD}
              -DGEANT4_INSTALL_DATA=OFF
              -DGEANT4_USE_SYSTEM_CLHEP=ON
              -DGEANT4_USE_XM=ON
              -DGEANT4_USE_GDML=ON
              -DGEANT4_USE_OPENGL=ON
              -DGEANT4_USE_OPENGL_X11=ON
              -DGEANT4_USE_RAYTRACER_X11=ON
              -DGEANT4_USE_NETWORKVRML=ON
              -DGEANT4_USE_NETWORKDAWN=ON
              -DGEANT4_USE_INVENTOR=OFF
              -DCMAKE_INSTALL_PREFIX=${CMAKE_SOURCE_DIR}/InstallArea/$ENV{CMTCONFIG}
              -DCMAKE_INSTALL_LIBDIR=lib
              -DGEANT4_INSTALL_DATADIR=${G4DATA}
              -DGEANT4_BUILD_VERBOSE_CODE=${G4VERBOSE}
              -DCMAKE_USE_CCACHE=${CMAKE_USE_CCACHE}
              -DCMAKE_USE_DISTCC=${CMAKE_USE_DISTCC}
              -DLCG_USE_NATIVE_COMPILER=${LCG_USE_NATIVE_COMPILER}
              -DCMAKE_MODULE_PATH=${Geant4Files_DIR}
)

# Hardcoded list of external packages used by Geant4, needed to prepare the
# runtime environment.
foreach(ext CLHEP EXPAT XercesC X11 OpenGL Motif)
  find_package(${ext})
endforeach()

# Copy the Geant4 sources as it was done with CMT
set(G4SRC ${CMAKE_CURRENT_BINARY_DIR}/Geant4-prefix/src/Geant4/source)
set(G4DST ${CMAKE_SOURCE_DIR}/Geant4)
configure_file(copy_sources.cmake.in copy_sources.cmake @ONLY)
install(SCRIPT ${CMAKE_CURRENT_BINARY_DIR}/copy_sources.cmake)
