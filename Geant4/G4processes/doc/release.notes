!-----------------------------------------------------------------------------
! Package     : Geant4/G4processes
! Responsible : Gloria CORTI, Nigel Watson
! Purpose     :
!-----------------------------------------------------------------------------

! 2015-11-02 - Marco Clemencic
 - Added .gitignore file

! ======================= G4processes v9r2 2015-07-24 ========================
! 2015-07-23 Tim Williams
 - Created patch of G4CollisionInitialState.cc to enable building with GCC4.9
   compiler, located in srcnew/models directory.
   Updated copyPatchedsource.py to copy patched G4CollisionInitialState.cc
   into place.

! ======================= G4processes v9r1 2015-04-24 ========================
! 2015-04-24 Nigel Watson
 - Include new alternative sources from Witek for kaon cross-sections

! ======================= G4processes v9r0 2015-03-06 ========================
! 2015-03-06 Nigel Watson
 - Ready for G4 9.6p4 releease

! 2014-11-05 - Nigel Watson/Tim Williams
 - Updated copyPatchedSource.py for 9.6.p03 (only single source to fix)

! 2014-03-05 - Nigel Watson
 - Updated requirements to:
   Adde models/hadronic/lepto_nuclear;
   Remove models/hadronic/photolepton_hadron, hadronic/models/incl;
   Do not add hadronic/models/abla, as this is junk/deprecated since 2010.

! ======================= G4processes v8r3p4 2013-12-19 ========================
! 2013-12-19 - Nigel Watson
 - Updated G4VPartonStringModel.cc, increase max. attempts before exception thrown (bug found by Paul Szczypka),
   should fix 0.5% of production job failures.

! ======================= G4processes v8r3p3 2013-05-16 ========================
! 2013-05-16 - Paul Szczypka
 - Updated G4FTFModel.cc to include fixes from the Geant4 team. Should fix the
   hanging jobs problem.

! ======================= G4processes v8r3p2 2013-04-10 ========================
! 2013-04-10 - Nigel Watson
 - Update copyPatchedSource.py, add srcnew_diffraction and .cc to fix (?) FTF
   mass problems.

! ======================= G4processes v8r3p1 2013-02-14 ========================
! 2013-02-14 - Nigel Watson
 - Remove copyPatchedIncludes.py, add copyPatchedSource.py, replaces .cc/.hh from ../srcnew
   (private version of G4 sources) into the InstallArea and package's include area.

! ======================= G4processes v8r3 2013-02-08 ========================
! 2013-02-08 - Nigel Watson
 - Added copyPatchedIncludes.py to put headers from ../srcnew (private version
   of G4 sources) into the InstallArea, updated requirements for this action.

! ======================= G4processes v8r2 2012-11-23 ========================
! 2012-11-23 - Nigel Watson
 - Tagged for release with G4 v95r2.

! 2012-06-18 - James Mccarthy
 - Removed  path for dna model. Not compatible with our version of CLHEP
 and not useful anyway. Can be included at a later date.

! 2012-06-17 - James Mccarthy
 - Added new path for LEND model, dna and INCL++ library in g4 v9.5.p01

! 2012-05-17 - James Mccarthy
 - Added new path for quasi_elastic library in g4 v9.5.p01

! ======================= G4processes v8r1 2011-07-14 ========================
! 2011-05-16 - Gloria Corti
 - Include in library new source directories introduced in geant4 9.4.patch01
   for PIXE simulation in electromagnetic
   . electromagnetic/pii
   and remove the one no lnger present
   . hadronic/models/leading_particle
 - Reorder linking of subdirectories according to what is in Geant4/processes
   GNUMakefile
 - Switch off the include path of this package to check if really needed

! 2011-03-02 - Hubert Degaudenzi
 - Added ignore properties in svn

! ======================= G4processes v8r0 2010-06-18 ========================
! 2010-06-18 - Gloria Corti
 - Include in library new source directories new in geant4 9.3.ref05 for
   Hadronic cross sections for antiparticle in CHIPS
   . hadronic/models/adjoint
   . hadronic/models/chiral_inv_phase_space/cross_sections
   . hadronic/models/chiral_inv_phase_space/fragmentation
   . hadronic/models/chiral_inv_phase_space/processes
   It needs G4particles v6r2.

! ====================== G4processes v7r1p1 2009-12-18 =======================
! 2009-12-08 - Hubert DEGAUDENZI
 - moved to the new GaudiPolicy for linker libraries, to allow deployement on
   window

!======================== G4processes v7r1 2009-11-03 ========================
! 2009-06-10 - Hubert DEGAUDENZI
 - Adapt to new new the external sources are imported.

!======================== G4processes v7r0 2008-06-30 ========================
! 2008-06-30 - Gloria CORTI
 - Include in library new source directories new in geant4 9.1
   (see details in geant4 9.1 release notes
   http://geant4.web.cern.ch/geant4/support/ReleaseNotes4.9.1.html)
   . hadronic/models/incl, Liege cascade INCL model, including ABLA evaporation
                           and fission
   . hadronic/models/qmd, QMD reaction model based on JQMD
   . hadronic/models/rpg,  Re-Parametrized Gheisha-style model

!======================== G4processes v6r0 2008-06-25 ========================
! 2008-06-16 - Gloria CORTI
 - Add in libray some more source subdirectory new in geant4 9.0
   . biasing
   . hadronic/models/lll_fission

!======================== G4processes v5r0 2008-06-09 ========================
! 2008-06-09 - Gloria CORTI
 - Remove code G4CascadeInterface.cc from srcnew since no longer used beacuase
   fix was introduced in Geant4 itself
 - Add in library some more source subdirectories even if not used in our
   physics lists:
   . scoring (new in 8.2)
   . electromagnetic/highenergy (new 7.0)
   . electromagnetic/polarization (new in 8.2)
   . hadronic/models/abrasion (new in 6.2)
   . hadronic/models/em_dissociation (new in 6.2)
   . hadronic/models/de_excitation/ablation (new in 6.2)
   Remove from library subdirectory em/photolepton_hadron (empty since 6.0)
   and use G4digits_hits

!========================== G4processes v4r3p1 ===============================
! 2007-07-25 - Hubert DEGAUDENZI
 - Split the library in 2 parts: otherwise there are too many symbols
   exported for the windows linker (>32768)

!======================= G4processes v4r3 =================================
! 2007-03-23 - Florence RANJARD
 - remove srcnew nolonger necessary with geant4.8.2.p01

!======================= G4processes v4r2 =================================
! 2007-01-17 - Florence Ranjard
 - remove include_dirs, get include files from InstallArea

!======================= G4processes v4r1 =================================
! 2005-11-07 - Gloria CORTI
 - introduce path to G4CascadeInterface to prevent crash when using it
   with problematic materials with fractional Z. Will remove it when
   materials will be described without it.

!======================= G4processes v4r0 =================================
! 2004-10-11 - Florence RANJARD
 - reuse G4processes to be able to build dynamic libraries on WIN32

!=========================================================================
