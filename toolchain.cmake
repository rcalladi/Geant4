# Set the version of HEPTools to use
set(heptools_version 84)

# this check is needed because the toolchain is called when checking the
# compiler (without the proper cache)
if(NOT CMAKE_SOURCE_DIR MATCHES "CMakeTmp")

  find_file(toolchain_macros NAMES GaudiToolchainMacros.cmake
            HINTS ${CMAKE_SOURCE_DIR}/cmake)
  if(toolchain_macros)
    get_filename_component(gaudi_modules_dir "${toolchain_macros}" PATH)
    set(CMAKE_MODULE_PATH ${gaudi_modules_dir} ${CMAKE_MODULE_PATH})
    include(GaudiToolchainMacros)
    init()
    include(UseHEPTools)
    use_heptools(${heptools_version})
  else()
    message(FATAL_ERROR "Cannot find GaudiToolchainMacros.cmake")
  endif()

endif()
