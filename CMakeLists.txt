CMAKE_MINIMUM_REQUIRED(VERSION 2.8.5)

# NO_NINJA : Geant4 build fails with Ninja

set(GEANT4_SRCS_REPOSITORY https://gitlab.cern.ch/lhcb/Geant4-srcs.git CACHE STRING "Repository for Geant4 sources")

find_package(GaudiProject)
gaudi_project(Geant4 v96r4p4
              DATA Geant4Files VERSION v96r*)

# FIXME: hack for compatibility with CMT build
file(WRITE ${CMAKE_BINARY_DIR}/dummy.cpp "")
link_directories(${CMAKE_INSTALL_PREFIX}/lib)
set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -Wl,--no-as-needed")

macro(back_comp_lib name)
  add_library(${name} SHARED ${CMAKE_BINARY_DIR}/dummy.cpp)
  add_dependencies(${name} Geant4)
  target_link_libraries(${name} ${ARGN})
  install(TARGETS ${name} DESTINATION lib)
endmacro()

back_comp_lib(G4processeshad G4processes)
back_comp_lib(G4externals G4zlib G4gl2ps)
